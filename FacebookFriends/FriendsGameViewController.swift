//
//  FriendsGameViewController.swift
//  FacebookFriends
//
//  Created by John Yang on 2/20/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import UIKit
import FBSDKCoreKit

class FriendsGameViewController: UIViewController {
    
    //Fake Profiles for practicing
    var names: [String] = ["A", "B", "C", "D", "E"]
    var profilePictures: [UIImage]!
    
    var friendsIDList = [String]() //Array that stores ID values
    var friendsNameList = [String]() //Array that stores Name values
    var profilePictureURL : String = ""
    var correctValue : Int = 0
    var proceed: Bool = false
    
    var counter: Int = 0
    var score: Int = 0
    var timer: NSTimeInterval!
    
    //UI Elements
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var scoreboard: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let color = UIColor(red: 151/255, green: 237/255, blue: 253/255, alpha: 1)
        self.view.backgroundColor = color
        getFriendsID()
        timer = NSDate.timeIntervalSinceReferenceDate()
        timerLabel.text = "\(timer)"
    }

    @IBAction func button1Clicked(sender: AnyObject) {
        if correctValue == 0 {
            label.text = "Correct!"
            button1.backgroundColor = UIColor.greenColor()
            score = score + 1
        } else {
            label.text = "Sorry, that was \(friendsNameList[correctValue])"
            button1.backgroundColor = UIColor.redColor()
        }
        getFriendsID()
    }
    
    @IBAction func button2Clicked(sender: AnyObject) {
        if correctValue == 1 {
            label.text = "Correct!"
            button2.backgroundColor = UIColor.greenColor()
            score += 1
        } else {
            label.text = "Sorry, that was \(friendsNameList[correctValue])"
            button2.backgroundColor = UIColor.redColor()
        }
        getFriendsID()
    }
    
    @IBAction func button3Clicked(sender: AnyObject) {
        if correctValue == 2 {
            label.text = "Correct!"
            button3.backgroundColor = UIColor.greenColor()
            score += 1
        } else {
            label.text = "Sorry, that was \(friendsNameList[correctValue])"
            button3.backgroundColor = UIColor.redColor()
        }
        getFriendsID()
    }
    
    @IBAction func button4Clicked(sender: AnyObject) {
        if correctValue == 3 {
            label.text = "Correct!"
            button4.backgroundColor = UIColor.greenColor()
            score += 1
        } else {
            label.text = "Sorry, that was \(friendsNameList[correctValue])"
            button4.backgroundColor = UIColor.redColor()
        }
        getFriendsID()
    }
    
    //Setup everything
    func instantiate() {
        /*
        print("CHECK \n------")
        print("Number of friends: \(friendsNameList.count)")
        print("CorrectValue: \(correctValue)")
        print(friendsNameList)
        print(profilePictureURL + "\n")
        */
        
        label.text = "Who is this?"
        counter += 1
        
        self.button1.setTitle("\(friendsNameList[0])", forState: UIControlState.Normal)
        self.button2.setTitle("\(friendsNameList[1])", forState: UIControlState.Normal)
        self.button3.setTitle("\(friendsNameList[2])", forState: UIControlState.Normal)
        self.button4.setTitle("\(friendsNameList[3])", forState: UIControlState.Normal)
        
        beautify(button1)
        beautify(button2)
        beautify(button3)
        beautify(button4)
        
        let url = NSURL(string: profilePictureURL)
        if let data = NSData(contentsOfURL: url!) {
            profilePic.image = UIImage(data: data)
        }
        profilePic.layer.borderWidth = 3
        profilePic.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    private func beautify(button: UIButton) {
        button.layer.borderColor = UIColor.blueColor().CGColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 10
        button.backgroundColor = UIColor.whiteColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getFriendsID() {
        scoreboard.text = "Score: \(score)/\(counter)"
        let friendsRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath:"/me/friends?limit=4", parameters:["fields": "friends"])
        
        friendsRequest.startWithCompletionHandler
        {
            (connection:FBSDKGraphRequestConnection!, result:AnyObject!, error: NSError!) -> Void in
            
            if error == nil {
                let resultDict = result as! NSDictionary
                let data : NSArray = resultDict.objectForKey("data") as! NSArray
                let data2 : NSDictionary = data[0].objectForKey("friends") as! NSDictionary
                let data3 : NSArray = data2.objectForKey("data") as! NSArray
                //print("NEW LINE \(data)") //for reading json data returned by completionhandler
                
                self.correctValue = Int(arc4random_uniform(UInt32(data3.count)))
                for index in 0..<data3.count {
                    let valueDict : NSDictionary = data3[index] as! NSDictionary
                    let id = valueDict.objectForKey("id") as! String
                    self.friendsIDList.append(id)
                    let name = valueDict.objectForKey("name") as! String
                    self.friendsNameList.append(name)
                    if self.correctValue == index {
                        self.profilePictureURL = "http://graph.facebook.com/\(id)/picture?type=large"
                    }
                }
                self.instantiate()
            } else {
                print("Error: \(error)")
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
